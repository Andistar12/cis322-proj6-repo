# ACP Control Time Calculator

This project calculates control open and close times for a given route. This calculator is based on Randonneurs USA regulations, and more information can be found [here](https://rusa.org/pages/acp-brevet-control-times-calculator). 

## Services (README)

I expose the following services on the following ports. Access them via `<host>:<port>`

* `5000`: Submit times into the database here. This is the same server and pages used in projects 4 and 5. This endpoint is NOT authenticated for convenience sake
* `80`: Consumer facing program for Project 6. Reads database entries through the API via AJAX. HTTP basic auth with user/pass or token
    * `/register`: Register a new account. No auth required
    * `/login`: Login to existing account. Input auth as login credentials
    * `/display`: Displays database entries through the API. Requires login 
    * `/logout`: Logs out of account. Requires login
* `5001`: REST API for Project 6. All endpoints protected via HTTP basic auth. Also handles account registration and token creation

The consumer facing UI has the following features:
* Remember me
* CSRF protection
* Login and logout functionality

## Application Architecture

This application does not depend on the credentials.ini file. It does not matter where it is located. This application has four containers:

* `server`: The same web server from project 4 and 5. Input times into the database here
* `website`: The consumer-facing program for project 6 and 7. This reads times from the database via the API
* `api`: The REST API that serves times from the database. Now protected via HTTP basic auth
* `db`: The mongo database

## API Documentation: Auth Endpoints

* `/api/register` registers a new user given the `username` and `password` as fields. Returns the username back
* `/api/token` generates a new auth token for the user. User must be HTTP basic authenticated with username and password

## API Documentation: Open and Close Times

All endpoints take a optional parameter `top` that limits how many entries are returned. Negative numbers indicate unlimited. Default -1. All endpoints are encrypted via HTTP basic auth (token only)

* `/listAll` and `/listAll/json` fetches all open and close times in JSON format
* `/listOpenOnly` and `/listOpenOnly/json` fetches all open times in JSON format
* `/listOpenOnly/csv` fetches all open times in CSV format
* `/listCloseOnly` and `/listCloseOnly/json` fetches all close times in JSON format
* `/listCloseOnly/csv` fetches all close times in CSV format

JSON response: depending on the end point, the API will return an `open` array, a `close` array, or both at the top level. Each is a string indicating the open or close time

CSV response: a raw, one-line CSV for each endpoint

## Test Cases [from Project 5]

1st test case: no data
*  Wipe the database of all entries. This is done on server start
*  Immediately click "Display"
*  The display page should come up, with zero control point entries

2nd test case: valid data
*  Wipe the database of all entries. This is done on server start
*  Select 200km as the brevet length
*  Enter `120` or any valid integer between zero and 200 into the km input
*  Hit Submit. The text on the right should display "Submitted"
*  Hit Display. The display page should come up, with one control point entry

3rd test case: Invalid data
*  Select 200km as the brevet length
*  Enter alpha characters as km input, for example "asdf"
*  Hit Submit. The result should be that zero entries are submitted
*  Enter a negative value as km input
*  Hit Submit. The result should be that zero entries are submitted
*  Hit Display. The display page should come up, with zero control point entries

## Usage

First, set your brevet distance and start time. Then, for each desired control point, enter each distance in miles or kilometers, and the corresponding open and close times will be automatically calculated. 

## Assumptions

Calculations are based on rules specified by RUSA. This section explains some specifics about the implementation algorithm:

* For opening times, the control location is clamped to be between zero and the brevet distance
* For closing times, the control location is max'd to be at least zero. If the control point is farther than the brevet length, then it is assumed to be the end of the brevet, and separate calculations will occur instead
* Invalid data passed in will default to the default as specified in the API
