"""
Replacement for RUSA ACP brevet time calculator
(see https://rusa.org/octime_acp.html)

"""

import os
import flask
import json
import logging
import arrow
from flask import request, g
from pymongo import MongoClient
from flask_httpauth import HTTPBasicAuth
from passlib.apps import custom_app_context as pwd_context
from itsdangerous import TimedJSONWebSignatureSerializer as Serializer
from itsdangerous import BadSignature, SignatureExpired

app = flask.Flask(__name__)
app.debug = os.environ["DEBUG"] == "true"
app.token_exp = int(os.environ["TOKEN_EXP"])
app.port = os.environ["API_PORT"]
app.secret_key = os.environ["SECRET_KEY"]
if app.debug:
    app.logger.setLevel(logging.DEBUG)

client = MongoClient(os.environ["DB_ADDR"], 27017)
db = client.appdb

auth = HTTPBasicAuth()

class User():
    def __init__(self, name, pass_hash):
        self.name = name
        self.pass_hash = pass_hash

def verify_auth_token(token):
    s = Serializer(app.secret_key)
    try:
        data = s.loads(token)
    except SignatureExpired:
        return None
    except BadSignature:
        return None
    except:
        return None
    return load_user(data["id"])
    
def load_user(username):
    # By username
    query = list(db.users.find({"username": username}))
    if len(query) == 0:
        return None
    user = query[0]
    return User(user["username"], user["password"]) # Pass is hashed in db

@auth.verify_password
def verify_user(username, password):
    # Note that the username can also be a token

    # Try token first
    user = verify_auth_token(username)
    if user is None:
        # Now try user + pass
        user = load_user(username)
        if user is None:
            return False
        try:
            verify = pwd_context.verify(password, user.pass_hash)
        except:
            return False
        if not verify:
            return False
    g.user = user
    return True 

class ListAll():
    def get(self):
        app.logger.debug("GET for list all")
        # Get parameter
        top = request.args.get("top", "")
        try:
            top = int(top)
        except:
            top = -1
        app.logger.debug("Received top=" + str(top))

        # Query database, then sort results
        query = db.ctimes.find()
        query = sorted(query, key = lambda x: x["km"])

        payload = {"open": [], "close": []}

        for count, entry in enumerate(query):
            if top >= 0 and count >= top:
                break

            open_time = arrow.get(entry["open"])
            close_time = arrow.get(entry["close"])

            ot = "{} {}".format(open_time.format("MM-DD"), open_time.format("HH:mm"))
            ct = "{} {}".format(close_time.format("MM-DD"), close_time.format("HH:mm"))

            payload["open"].append(ot)
            payload["close"].append(ct)

        app.logger.debug("Returning " + str(payload))
        return payload

class ListOpenOnly(ListAll):
    def get(self):
        app.logger.debug("GET for list open only")
        # Use inheritance to get everything, then limit by top
        payload = super(ListOpenOnly, self).get()
        del payload["close"]
        app.logger.debug("Returning " + str(payload))
        return payload

class ListCloseOnly(ListAll):
    def get(self):
        # Use inheritance to get everything, then limit by top
        app.logger.debug("GET for list close only")
        payload = super(ListCloseOnly, self).get()
        del payload["open"]
        app.logger.debug("Returning " + str(payload))
        return payload

# Define JSON routes
@app.route("/listAll")
@app.route("/listAll/")
@app.route("/listAll/json")
@auth.login_required
def listAll():
    return flask.jsonify(ListAll().get())

@app.route("/listOpenOnly")
@app.route("/listOpenOnly/")
@app.route("/listOpenOnly/json")
@auth.login_required
def listOO():
    return flask.jsonify(ListOpenOnly().get())

@app.route("/listCloseOnly")
@app.route("/listCloseOnly/")
@app.route("/listCloseOnly/json")
@auth.login_required
def listCO():
    return flask.jsonify(ListCloseOnly().get())

# Use Flask itself to declare CSV endpoints, grab data, then reformat
@app.route("/listOpenOnly/csv")
@auth.login_required
def listOpenOnlyCsv():
    app.logger.debug("GET for list open only csv")
    data = ListOpenOnly().get()
    payload = ", ".join([elem for elem in data["open"]]) + "\n"
    app.logger.debug("Returning " + str(payload))
    return flask.make_response(payload)

@app.route("/listCloseOnly/csv")
@auth.login_required
def listCloseOnlyCsv():
    app.logger.debug("GET for list close only csv")
    data = ListCloseOnly().get()
    payload = ", ".join([elem for elem in data["close"]]) + "\n"
    app.logger.debug("Returning " + str(payload))
    return flask.make_response(payload)

@app.route("/api/register", methods=["POST"])
def register_endpoint():
    username = request.args.get("username")
    password = request.args.get("password")

    if not username or not password:
        return flask.jsonify({"error": "Empty username or password"}), 400

    # Hash password
    password = pwd_context.encrypt(password)

    app.logger.debug("Attempting to register user " + username)

    query = list(db.users.find({"username": username}))
    if len(query) > 0:
        return flask.jsonify({"error": "Username already exists"}), 400

    data = {
        "username": username,
        "password": password
    }
    db.users.insert_one(data)
    resp = flask.Response(json.dumps({"username": username}))
    resp.headers["Location"] = "/users/" + username
    return resp, 201

@app.route("/api/token")
@auth.login_required
def get_token():
    s = Serializer(app.secret_key, expires_in=app.token_exp)
    token = s.dumps({"id": g.user.name})
    result = {
        "token": token.decode("utf-8"),
        "duration": app.token_exp
    }
    return flask.jsonify(result)

if __name__ == "__main__":
    print("Opening for global access on port {}".format(app.port))
    app.run(port=app.port, host="0.0.0.0")
