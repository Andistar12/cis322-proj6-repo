"""
Replacement for RUSA ACP brevet time calculator
(see https://rusa.org/octime_acp.html)

"""
import flask
import requests
from flask import request, redirect, url_for
import os
import json
import logging
from flask_wtf import FlaskForm, CSRFProtect
from wtforms import StringField, PasswordField, validators
from flask_login import LoginManager, UserMixin, login_required, current_user, login_user, logout_user
from pymongo import MongoClient

###
# Globals
###
app = flask.Flask(__name__)
app.secret_key = "dummy" # Temp for now

csrf = CSRFProtect()
app.WTF_CSRF_CHECK_DEFAULT = False
login_manager = LoginManager()
login_manager.login_view = "login"
login_manager.login_message = u"Please log in to access this page"
login_manager.refresh_view = "reauth"
login_manager.needs_refresh_message = (u"To protect your account, please relogin")
client = MongoClient(os.environ["DB_ADDR"], 27017)
db = client.userdb

class User(UserMixin):
    def __init__(self, name, token, active=True):
        self.name = name
        self.active = active
        self.token = token

    def get_id(self):
        return self.name

    def is_active(self):
        return self.active

    def refresh_token(self):
        req = requests.get("http://api/api/token", auth=(self.token, None))
        if req.status_code == 401:
            # Bad token somehow
            return False

        # Stick user, token into db
        token = json.loads(req.content)["token"]
        db.users.delete_many({"username": self.name})
        data = {
            "username": self.name,
            "token": token
        }
        db.users.insert_one(data)
        self.token = token
        return True

@login_manager.user_loader
def user_loader(username):
    query = list(db.users.find({"username": username}))
    if len(query) == 0:
        return None
    return User(username, query[0]["token"])

# def form
class RegForm(FlaskForm):
    name_first = StringField("Userrname", [validators.DataRequired()])
    password = PasswordField("New Password", [validators.DataRequired(), validators.EqualTo("confirm", message="Passwords must match")])
    confirm = PasswordField("Confirm Password")

class LoginForm(FlaskForm):
    name_first = StringField("Userrname", [validators.DataRequired()])
    password = PasswordField("Password", [validators.DataRequired()])

###
# Auth routes
###

@app.route("/register", methods=["GET", "POST"])
@app.route("/register.html", methods=["GET", "POST"])
def register(message=""):
    if current_user.is_authenticated:
        return redirect(url_for("display"))
    form = RegForm(flask.request.form)
    if flask.request.method == "POST": 
        if not form.validate_on_submit():
            return flask.render_template("register.html", form=form, 
                    message="Error: passwords must match and data must be filled in"), 400
                
        # Register user now
        app.logger.debug("Attempting to register user")
        params = {
            "username": form.name_first.data,
            "password": form.password.data # TODO move hashing to here instead of backend
        }

        # Refer to API
        req = requests.post("http://api/api/register", params=params)
        app.logger.debug(f"Received: {req.content}")
        payload = json.loads(req.content)
        if "error" in payload:
            msg = "Error: " + payload["error"]
            return flask.render_template("register.html", form=form, 
                    message=msg), 400
        
        return redirect(url_for("login"))
    app.logger.debug("Returning original form")
    return flask.render_template("register.html", form=form, message=""), 201

@app.route("/", methods=["GET", "POST"])
@app.route("/index", methods=["GET", "POST"])
@app.route("/login", methods=["GET", "POST"])
@app.route("/login.html", methods=["GET", "POST"])
def login(message=""):
    if current_user.is_authenticated:
        return redirect(url_for("display"))
    form = LoginForm(flask.request.form)
    if flask.request.method == "POST": 
        if not form.validate_on_submit():
            return flask.render_template("login.html", form=form, 
                    message="Error: data must be filled in"), 400
    
        username = form.name_first.data
        password = form.password.data
        req = requests.get("http://api/api/token", auth=(username, password))
        if req.status_code == 401:
            # Bad credentials
            return flask.render_template("login.html", form=form, 
                    message="Error: invalid credentials"), 400

        # Stick user, token into db
        app.logger.debug(f"Received: {req.content}")
        token = json.loads(req.content)["token"]
        db.users.delete_many({"username": username})
        data = {
            "username": username,
            "token": token
        }
        db.users.insert_one(data)
        user = User(username, token)
        if login_user(user, remember="yes"):
            return redirect(url_for("display"))

        return flask.render_template("login.html", form=form, 
                message="Error: please try again"), 400

    app.logger.debug("Returning original form")
    return flask.render_template("login.html", form=form, message=""), 201


@app.route("/logout")
@login_required
def logout():
    logout_user()
    return redirect(url_for("login"))

###
# Pages
###

@app.route("/display")
@app.route("/display.html")
@login_required
def display():
    app.logger.debug("Main page entry")
    return flask.render_template('display.html')

@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("login")
    return flask.render_template('404.html'), 404

###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#   These reroute to our api
#
###############

@app.route("/listAll")
@app.route("/listAll/")
@app.route("/listAll/json")
@login_required
def listAll():
    user = current_user
    if not user.refresh_token():
        # Session expired
        return "Token error"
    params = {
        "top": request.args.get("top", -1)
    }
    return requests.get("http://api/api/listAll", params=params, auth=(user.token, None)).content

@app.route("/listOpenOnly")
@app.route("/listOpenOnly/")
@app.route("/listOpenOnly/json")
@login_required
def listOpenOnly():
    user = current_user
    if not user.refresh_token():
        # Session expired
        return "Token error"
    params = {
        "top": request.args.get("top", -1)
    }
    req = requests.get("http://api/listOpenOnly", params=params, auth=(user.token, None),
            headers={key: value for (key, value) in request.headers if key != "Host"})
    return req.content

@app.route("/listCloseOnly")
@app.route("/listCloseOnly/")
@app.route("/listCloseOnly/json")
@login_required
def listCloseOnly():
    user = current_user
    if not user.refresh_token():
        # Session expired
        return "Token error"
    params = {
        "top": request.args.get("top", -1)
    }
    req = requests.get("http://api/listCloseOnly", params=params, auth=(user.token, None),
            headers={key: value for (key, value) in request.headers if key != "Host"})
    return req.content

@app.route("/listOpenOnly/csv")
@login_required
def listOpenOnlyCSV():
    user = current_user
    if not user.refresh_token():
        # Session expired
        return "Token error"
    params = {
        "top": request.args.get("top", -1)
    }
    # Wrap in JSON for jQuery
    req = requests.get("http://api/listOpenOnly/csv", params=params, auth=(user.token, None),
            headers={key: value for (key, value) in request.headers if key != "Host"})
    return flask.jsonify({"data": req.content.decode("utf-8")})

@app.route("/listCloseOnly/csv")
@login_required
def listCloseOnlyCSV():
    user = current_user
    if not user.refresh_token():
        # Session expired
        return "Token error"
    params = {
        "top": request.args.get("top", -1)
    }
    req = requests.get("http://api/listCloseOnly/csv", params=params, auth=(user.token, None),
            headers={key: value for (key, value) in request.headers if key != "Host"})
    return flask.jsonify({"data": req.content.decode("utf-8")})

#############

app.debug = os.environ["DEBUG"] == "true"
app.port = os.environ["SERVER_PORT"]
if app.debug:
    app.logger.setLevel(logging.DEBUG)

login_manager.setup_app(app)

if __name__ == "__main__":
    print("Opening for global access on port {}".format(app.port))
    app.run(port=app.port, host="0.0.0.0")
