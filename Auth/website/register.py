import os
import flask
import json
import arrow
import logging
from passlib.apps import custom_app_context as pwd_context
from flask_wtf import FlaskForm, CSRFProtect
from wtforms import SubmitField, StringField, PasswordField, validators
from api import app, client, db

# Configure login and database
csrf = CSRFProtect()
app.WTF_CSRF_CHECK_DEFAULT = False

@login_manger.user_loader
def load_user(username):
    # By username
    query = list(db.users.find({"username": username}))
    if len(query) == 0:
        return None
    user = query[0]
    return User(user["username"], user["password"]) # Pass is hashed in db



# def form
class RegForm(FlaskForm):
    name_first = StringField("Userrname", [validators.DataRequired()])
    password = PasswordField("New Password", [validators.DataRequired(), validators.EqualTo("confirm", message="Passwords must match")])
    confirm = PasswordField("Confirm Password")

class LoginForm(FlaskForm):
    name_first = StringField("Userrname", [validators.DataRequired()])
    password = PasswordField("New Password", [validators.DataRequired()])

def register():
    form = RegForm(flask.request.form)
    message = ""
    if flask.request.method == "POST": 
        if not form.validate_on_submit():
            return flask.render_template("register.html", form=form, 
                    message="Error: passwords must match and data must be filled in"), 400
                
        # Register user now
        app.logger.debug("Attempting to register user")
        username = form.name_first.data
        query = list(db.users.find({"username": username}))
        if len(query) > 0:
           return flask.render_template("register.html", form=form,
                   message="Error: username already exists")
        password = pwd_context.encrypt(form.password.data)
        data = {
            "username": username,
            "password": password
        }
        db.users.insert_one(data)
        resp = flask.Response(json.dumps({"username": username}))
        resp.headers["Location"] = "/users/" + username
        return resp, 201
    app.logger.debug("Returning original form")
    return flask.render_template("register.html", form=form, message=""), 201

def login():
    form = LoginForm(flask.request.form)
    message = ""
    if flask.request.method == "POST": 
        if not form.validate_on_submit():
            return flask.render_template("register.html", form=form, 
                    message="Error: data must be filled in"), 400
                
        # Login user now
        app.logger.debug("Attempting to login user")
        username = form.name_first.data

        query = list(db.users.find({"username": username}))
        if len(query) > 0:
           return flask.render_template("register.html", form=form,
                   message="Error: username already exists")
        password = pwd_context.encrypt(form.password.data)
        data = {
            "username": username,
            "password": password
        }
        db.users.insert_one(data)
        resp = flask.Response(json.dumps({"username": username}))
        resp.headers["Location"] = "/users/" + username
        return resp, 201
    app.logger.debug("Returning original form")
    return flask.render_template("login.html", form=form, message=""), 201


@app.before_request
def check_csrf():
    if flask.request.endpoint == "/register":
        app.logger.debug("Applying CSRF protection to /register")
        csrf.protect()

csrf.init_app(app)

app.logger.debug("Login stuff setup")
