import arrow
import nose 
from acp_times import open_time, close_time


"""
Method signatures
def open_time(control_dist_km, brevet_dist_km, brevet_start_time):
    Args:
       control_dist_km:  number, the control distance in kilometers
       brevet_dist_km: number, the nominal distance of the brevet
           in kilometers, which must be one of 200, 300, 400, 600,
           or 1000 (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control open time.
       This will be in the same time zone as the brevet start time.


def close_time(control_dist_km, brevet_dist_km, brevet_start_time):
    Args:
       control_dist_km:  number, the control distance in kilometers
          brevet_dist_km: number, the nominal distance of the brevet
          in kilometers, which must be one of 200, 300, 400, 600, or 1000
          (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control close time.
       This will be in the same time zone as the brevet start time.
"""

def test_open_time():
    """
    Performs tests for open_time
    These tests only vary the first two parameters as arrow is supposed to
    handle the logic for the third one (that is, ISO 8601 format)
    """

    curr = arrow.now()

    # Example 1 from RUSA
    assert open_time(0, 200, curr.isoformat()) == \
            curr.shift(hours=+0, minutes=+0).isoformat()
    assert open_time(60, 200, curr.isoformat()) == \
            curr.shift(hours=+1, minutes=+46).isoformat()
    assert open_time(120, 200, curr.isoformat()) == \
            curr.shift(hours=+3, minutes=+32).isoformat()
    assert open_time(175, 200, curr.isoformat()) == \
            curr.shift(hours=+5, minutes=+9).isoformat()
    assert open_time(205, 200, curr.isoformat()) == \
            curr.shift(hours=+5, minutes=+53).isoformat()

    # Example 2 from RUSA
    assert open_time(100, 600, curr.isoformat()) == \
            curr.shift(hours=+2, minutes=+56).isoformat()
    assert open_time(200, 600, curr.isoformat()) == \
            curr.shift(hours=+5, minutes=+53).isoformat()
    assert open_time(350, 600, curr.isoformat()) == \
            curr.shift(hours=+10, minutes=+34).isoformat()
    assert open_time(550, 600, curr.isoformat()) == \
            curr.shift(hours=+17, minutes=+8).isoformat()

    # Example 3 from RUSA
    assert open_time(890, 1000, curr.isoformat()) == \
            curr.shift(hours=+29, minutes=+9).isoformat()

    # Edge case: starting control of zero
    assert open_time(0, 200, curr.isoformat()) == \
            curr.shift(hours=+0, minutes=+0).isoformat()
    assert open_time(0, 300, curr.isoformat()) == \
            curr.shift(hours=+0, minutes=+0).isoformat()
    assert open_time(0, 400, curr.isoformat()) == \
            curr.shift(hours=+0, minutes=+0).isoformat()
    assert open_time(0, 600, curr.isoformat()) == \
            curr.shift(hours=+0, minutes=+0).isoformat()
    assert open_time(0, 1000, curr.isoformat()) == \
            curr.shift(hours=+0, minutes=+0).isoformat()

    # Made up example
    assert open_time(100, 400, curr.isoformat()) == \
            curr.shift(hours=+2, minutes=+56).isoformat()
    assert open_time(150, 400, curr.isoformat()) == \
            curr.shift(hours=+4, minutes=+25).isoformat()
    assert open_time(251, 400, curr.isoformat()) == \
            curr.shift(hours=+7, minutes=+29).isoformat()
    assert open_time(350, 400, curr.isoformat()) == \
            curr.shift(hours=+10, minutes=+34).isoformat()
    assert open_time(400, 400, curr.isoformat()) == \
            curr.shift(hours=+12, minutes=+8).isoformat()
    assert open_time(410, 400, curr.isoformat()) == \
            curr.shift(hours=+12, minutes=+8).isoformat()
    assert open_time(435, 400, curr.isoformat()) == \
            curr.shift(hours=+12, minutes=+8).isoformat()


def test_close_time():
    """
    Performs tests for open_time
    These tests only vary the first two parameters as arrow is supposed to
    handle the logic for the third one (that is, ISO 8601 format)
    """

    curr = arrow.now()

    # Example 1 from RUSA
    assert close_time(0, 200, curr.isoformat()) == \
            curr.shift(hours=+1, minutes=+0).isoformat()
    assert close_time(60, 200, curr.isoformat()) == \
            curr.shift(hours=+4, minutes=+0).isoformat()
    assert close_time(120, 200, curr.isoformat()) == \
            curr.shift(hours=+8, minutes=+00).isoformat()
    assert close_time(175, 200, curr.isoformat()) == \
            curr.shift(hours=+11, minutes=+40).isoformat()
    assert close_time(205, 200, curr.isoformat()) == \
            curr.shift(hours=+13, minutes=+30).isoformat()

    # Example 2 from RUSA
    assert close_time(550, 600, curr.isoformat()) == \
            curr.shift(hours=+36, minutes=+40).isoformat()
    assert close_time(600, 600, curr.isoformat()) == \
            curr.shift(hours=+40, minutes=+0).isoformat()

    # Example 3 from RUSA
    assert close_time(890, 1000, curr.isoformat()) == \
            curr.shift(hours=+65, minutes=+23).isoformat()

    # Edge case: sub 60 km controls
    assert close_time(0, 1000, curr.isoformat()) == \
            curr.shift(hours=+1, minutes=+0).isoformat()
    assert close_time(20, 1000, curr.isoformat()) == \
            curr.shift(hours=+2, minutes=+0).isoformat()
    assert close_time(60, 1000, curr.isoformat()) == \
            curr.shift(hours=+4, minutes=+0).isoformat()

    # Made up example
    assert close_time(100, 400, curr.isoformat()) == \
            curr.shift(hours=+6, minutes=+40).isoformat()
    assert close_time(150, 400, curr.isoformat()) == \
            curr.shift(hours=+10, minutes=+00).isoformat()
    assert close_time(251, 400, curr.isoformat()) == \
            curr.shift(hours=+16, minutes=+44).isoformat()
    assert close_time(350, 400, curr.isoformat()) == \
            curr.shift(hours=+23, minutes=+20).isoformat()
    assert close_time(400, 400, curr.isoformat()) == \
            curr.shift(hours=+27, minutes=+0).isoformat()
    assert close_time(410, 400, curr.isoformat()) == \
            curr.shift(hours=+27, minutes=+0).isoformat()
    assert close_time(435, 400, curr.isoformat()) == \
            curr.shift(hours=+27, minutes=+0).isoformat()

